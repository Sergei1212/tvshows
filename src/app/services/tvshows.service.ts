import { Injectable } from "@angular/core";
import { Tvshow } from '../models/data';


@Injectable()

export class TvshowService {
    public tvshows: Tvshow[];

    getTvshowsBydate(date: string): Tvshow[] {
        return this.tvshows.filter(tvshow => tvshow.date === date);
    }




}