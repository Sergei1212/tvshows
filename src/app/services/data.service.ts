import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Channel, Tvshow, LiveChannel } from '../models/data';
import * as moment from 'moment';


@Injectable()
export class DataService {
    private readonly BASE_URL = 'https://api.persik.by/v2/';//readonly- запрещаем чтение из вне.
    private channels: Channel[] = [];

    constructor(private http: HttpClient) { }

    getChannels(): Promise<Channel[]> {
        return new Promise(resolve => {
            if (this.channels.length > 0) {
                resolve(this.channels);
            } else {
                return this.http.get<ServerChannels>(this.BASE_URL.concat('content/channels'))
                    .pipe(map(res => res.channels))
                    .toPromise()
                    .then(res => {
                        if (res) {
                            this.channels = res;
                            resolve(res);
                        } else {
                            resolve([]);
                        }
                    });//в res упадет ответ от сервиса и из нее забираем массив channels
            }
        });
    }

    getTvShowsById(channel_id: number): Promise<Tvshow[]> {
        const params: HttpParams = new HttpParams().set('channels[]', channel_id.toString());
        return this.http.get<ServerTvshows>(this.BASE_URL.concat('epg/tvshows'), { params }).pipe(map(res => res.tvshows.items)).toPromise();
    }

    getCurrentTvshow(channel_id: number): Promise<Tvshow> {
        return new Promise(resolve => {
            const currentTime = moment().unix();
            this.getTvShowsById(channel_id).then(tvshows => {
                const currentTvshow: Tvshow = tvshows.find(tvshow => {
                    return tvshow.start < currentTime && tvshow.stop >= currentTime;
                });
                resolve(currentTvshow);
            });
        });
    }



}
interface ServerChannels {
    channels: Channel[];
}
interface ServerTvshows {
    tvshows: {
        items: Tvshow[];
        total: number;
    };
}