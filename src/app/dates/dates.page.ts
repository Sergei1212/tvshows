import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { Tvshow } from '../models/data';
import { TvshowService } from '../services/tvshows.service';
import { LoadingController } from '@ionic/angular';

@Component({
    selector: 'dates',
    templateUrl: 'dates.page.html',
    styleUrls: ['dates.page.scss']
})

export class DatesPage implements OnInit {

    public uniqueDates: string[] = [];
    private loader: HTMLIonLoadingElement;

    constructor(
        private activatedRoute: ActivatedRoute,
        private dateService: DataService,
        private tvshowService: TvshowService,
        public loadingController: LoadingController
        ) {}

    ngOnInit() {
        this.startLoadIndicate();//колесо загрузки
        this.activatedRoute.queryParams.subscribe(params => {
            const id = params['id'];
            if(id) {
                this.dateService.getTvShowsById(+id).then(res => {
                    //console.log('Result: ', res);
                    this.tvshowService.tvshows = res;
                    this.uniqueDates = res.map(item => item.date).filter((date, index, array) =>  {
                        return array.indexOf(date) ===index;
                    });
                });
            }
        });
        this.stopLoadIndicate();//закрываем загрузку.
    }

    public get isHaveData(): boolean {
        return this.uniqueDates.length > 0;
    }

    private async startLoadIndicate() {//async и await - см. https://ionicframework.com/docs/api/loading    
        this.loader = await this.loadingController.create({
          message: 'Загрузка передач.',
        });
        this.loader.present();
      }
    
      private stopLoadIndicate() {
        setTimeout(() => {
          this.loader.dismiss();
        }, 1000);
      }

  

}