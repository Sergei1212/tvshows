import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { DatesPage } from './dates.page';
import { DateComponent } from '../components/date/date.component';
import { PipeModule } from '../pipes/pipe.module';
import { NotFoundModule } from '../components/not-found/not-found.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundModule,
    PipeModule,
    RouterModule.forChild([
      {
        path: '',
        component: DatesPage
      }
    ])
  ],
  declarations: [
    DatesPage,
    DateComponent
  ]
})
export class DatesPageModule { }
