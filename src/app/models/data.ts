import { NumberValueAccessor } from '@angular/forms/src/directives';

export interface Channel {
    channel_id: number;
    name: string;
    logo: string;
    genres: number[];
    streamUrl: string;
}

export interface Tvshow {
    channel_id: number;
    date: string;
    deleted: number;
    start: number;
    stop: number;
    title: string;
    ts: number;
    tvshow_id: string;
}

export interface LiveChannel {
    id: number;
    name: string;
    satrt: number;
    stop: number;
    logo: string;
    show_name: string;
}

