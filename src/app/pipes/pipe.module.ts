import { NgModule } from "@angular/core";
import { DatePipe } from './date.pipe';
import { TimePipe } from './time.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        DatePipe,
        TimePipe
    ],
    imports: [
        CommonModule
    ],
    exports: [
        DatePipe,
        TimePipe
    ]
})

export class PipeModule {}