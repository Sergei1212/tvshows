import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Channel } from '../models/data';
import { LoadingController } from '@ionic/angular';
import { LoadedRouterConfig } from '@angular/router/src/config';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public channels: Channel[] = [];
  private loader:HTMLIonLoadingElement;

  constructor(
    private dataService: DataService,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.startLoadIndicate();
    this.dataService.getChannels().then(res => {
      //console.log(res);
      //console.log('load finished');
      this.channels = res;
      this.stopLoadIndicate();
    });
  }

  private async startLoadIndicate() {//async и await - см. https://ionicframework.com/docs/api/loading    
    this.loader = await this.loadingController.create({
      message: 'Загрузка',
    });
    this.loader.present();
  }

  private stopLoadIndicate() {
    setTimeout(() => {
      this.loader.dismiss();
    }, 1000);
  }

}
