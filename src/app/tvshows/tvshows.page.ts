import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { TvshowService } from '../services/tvshows.service';
import { Tvshow } from '../models/data';


@Component({
    selector: 'tvshows',
    templateUrl: 'tvshows.page.html',
    styleUrls: ['tvshows.page.scss']
})

export class TvshowsPage implements OnInit {

    public tvshows: Tvshow[] = [];
    
    constructor(private activeRoute: ActivatedRoute, private tvshowsService: TvshowService) {}

    ngOnInit() {
        this.activeRoute.queryParams.subscribe(params => {
            if (params['date']) {
                this.tvshows = this.tvshowsService.getTvshowsBydate(params['date']);
            }
        });
    }
/*
    public isCurrent(): boolean {
        return moment(this.date).isSame(moment().format('YYYY-MM-DD'));
    }
*/

}