import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import * as Hls from "hls.js";

@Component({
    selector: 'player-page',
    templateUrl: 'player.page.html',
    styleUrls: ['player.page.scss']
})

export class PlayerPage implements OnInit {

    private channel_id: number;

    constructor(private activatedRoute: ActivatedRoute, private dataService: DataService) { }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(res => {
            this.channel_id = +res['channel_id'];
            this.getStreamUrl().then(url => {
                console.log('StreamUrl: ', url);
            }).catch(err => {
                console.log('Error: ', err);
            });
        });

        const video = <HTMLVideoElement>document.getElementById('player');
        const hls = new Hls();
        hls.loadSource('https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8');
        hls.attachMedia(video);
        hls.on(Hls.Events.MANIFEST_PARSED, function () {
            video.play();
        });



    }

    private getStreamUrl(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.dataService.getChannels().then(chs => {
                const channel = chs.find(ch => +ch.channel_id === this.channel_id);
                if (channel) {
                    resolve(channel.streamUrl);
                } else {
                    reject('No stream url');
                }
            });
        });

    }

}