import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
    selector: 'date',
    templateUrl: 'date.component.html',
    styleUrls: ['date.component.scss'],
})
export class DateComponent implements OnInit, AfterViewInit {

    @Input() date: string;

    constructor(private router: Router) { }

    ngOnInit() { }

    ngAfterViewInit() {
        if (this.isCurrent) {
            setTimeout(() => {//завернули всю функцию в сеттаймаут что бы выполнить ее асинхронно, независимо от загрузки контента.
                const active = document.getElementsByClassName('date-string_active')[0];
                //console.log(active);
                active.scrollIntoView({ block: 'center', behavior: 'smooth' });
            }, 0);

        }
    }

    get isCurrent(): boolean {
        return moment(this.date).isSame(moment().format('YYYY-MM-DD'));
    }

    public showTvshows() {
        this.router.navigate(['/tvshows'], { queryParams: { date: this.date }});
    }

}