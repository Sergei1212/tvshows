import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
    declarations: [
        NotFoundComponent    
      ],
  imports: [
    CommonModule,
    IonicModule   
  ],
  exports: [
      NotFoundComponent
  ]
  
})
export class NotFoundModule {}
