import { Component, OnInit, Input, AfterViewInit, OnDestroy } from "@angular/core";
import { Tvshow } from 'src/app/models/data';
import * as moment from 'moment';
import { Router } from '@angular/router';

@Component({
    selector: 'tvshow',
    templateUrl: 'tvshow.component.html',
    styleUrls: ['tvshow.component.scss']
})

export class TvshowComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() tvshow: Tvshow;
    public progress: number;
    private readonly timeInterval = 3000;
    private timer: any;

    constructor(private router: Router) { }

    ngOnInit() {
        if (this.isCurrent) {
            this.progress = this.calculateProgress();
        }
        this.startTimer();
    }

    private startTimer(): void {
        this.timer = setInterval(() => {
            if (this.isCurrent) {
                this.progress = this.calculateProgress();
            } else {
                clearInterval(this.timer);
            }
        }, this.timeInterval);
    }

    public get isCurrent(): boolean {
        return moment().isBetween(moment.unix(this.tvshow.start), moment.unix(this.tvshow.stop));

    }

    ngAfterViewInit() {
        if (this.isCurrent) {
            setTimeout(() => {
                const item = document.getElementsByClassName('tvshow_active')[0];
                item.scrollIntoView({ block: 'center', behavior: 'smooth' });
            }, 0);
        }
    }

    public showStream(): void {
        if (this.isCurrent) {
            this.router.navigate(['/player'], {queryParams: {channel_id: this.tvshow.channel_id}});
        }

    }

    private calculateProgress(): number {
        return (moment().unix() - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start);
    }

    ngOnDestroy() {
        clearInterval(this.timer);
    }



}