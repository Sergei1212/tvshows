import { Component, OnInit } from "@angular/core";
import { DataService } from '../services/data.service';
import { Channel } from '../models/data';
import { LoadingController } from '@ionic/angular';

@Component({
    selector: 'live',
    templateUrl: 'live.page.html',
    styleUrls: ['live.page.scss']
})

export class LivePage implements OnInit {

    public channels: Channel[] = [];
    private loader: HTMLIonLoadingElement;

    constructor(private dataService: DataService, private loadingController: LoadingController) { }

    ngOnInit() {
        this.startLoadIndicate();
        this.dataService.getChannels().then(res => {
            this.channels = res;
            this.stopLoadIndicate();
        });
    }

    public get isHaveData(): boolean {
        return this.channels.length > 0;
    }

    private async startLoadIndicate() {//async и await - см. https://ionicframework.com/docs/api/loading    
        this.loader = await this.loadingController.create({
            message: 'Загрузка передач',
        });
        await this.loader.present();
    }

    private stopLoadIndicate() {
        setTimeout(() => {
            this.loader.dismiss();
        }, 1000);
    }
}